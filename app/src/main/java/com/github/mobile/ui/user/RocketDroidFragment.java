package com.github.mobile.ui.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mobile.R;
import com.github.mobile.ui.DialogFragment;

public class RocketDroidFragment extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rocketdroid_tab, null);
    }
}
